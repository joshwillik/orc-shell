use parser::Expression;

pub struct Runtime {
    state: State,
}

impl Runtime {
    pub exec(&mut self, statement: &Statement) -> Return {
        println!("running {:?}", statement);
    }
}

pub enum Return {
    Exit(i32),
}
