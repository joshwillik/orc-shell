extern crate nom;
extern crate failure;
use std::io::{Stdin, Stdout, Write};
use std::process::{Command};
use failure::Error;
use crate::runtime::{Expression};
use crate::parser::{Result as ParseResult};

pub struct Repl {
    runtime: Runtime,
    input: Stdin,
    output: Stdout,
    buffer: String,
}

impl Repl {
    pub fn new(stdin: Stdin, stdout: Stdout) -> Self {
        Self{
            runtime: Runtime::new(),
            input: stdin,
            output: stdout,
            buffer: String::new(),
        }
    }
    pub fn run(&mut self) -> Result<i32, Error> {
        loop {
            for statement in self.read()? {
                match self.runtime.run(&statement) {
                    Exit(code) => Ok(i32),
                }
            }
            self.read();
        }
    }
    fn read(&mut self) -> Result<Vec<Expression>, Error> {
        let mut line = String::new();
        let mut n_lines = 0;
        loop {
            if n_lines == 0 {
                print!("> ");
            } else {
                print!("  ");
            }
            self.output.flush()?;
            n_lines += 1;
            self.input.read(&mut line)?;
            match parser::parse(&line) {
                ParseResult::Ok(statements) => return statements,
                ParseResult::Err(error) => eprintln!("{:?}", err),
                ParseResult::Incomplete => (),
            };
        }
    }
}
