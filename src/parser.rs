pub use nom::{IResult, Err};
use nom::{
    sequence::tuple,
    multi::*,
    sequence::*,
    branch::*,
    character::streaming::*,
    bytes::streaming::*,
};

pub enum Result {
    Ok(Vec<Expression>),
    Err(Error),
    Incomplete,
}

pub fn parse(i: &String) -> Result {
    match expressions(i) {
        IResult::Ok(statements) => return Result::Ok(statements),
        IResult::Err(Failure(e)) => return Result::Err(e),
        IResult::Err(_) => Result::Incomplete,
    }
}

fn expressions(i: &str) -> IResult<Vec<Expression>> {
    separated_list0(tag(";"), expression)
}

fn expression(i: &str) -> IResult<&str, Expression> {
    alt((subshell, command))(i)
}

fn subshell(i: &str) -> IResult<&str, Expression> {
    let (input, expr) = delimited(char('('), expression, char(')'))?;
    Ok(Expression::Subshell(expr))
}

fn command(i: &str) -> IResult<&str, Expression> {
    let (input, (command, args, _)) = tuple((word,
        many0(preceded(one_of("\t "), word)),
        alt((line_ending, is_a(";")))))(i)?;
    Ok((input, Expression {
        command: command.to_string(),
        args: args.iter().map(|a| a.to_string()).collect(),
    }))
}

fn word(i: &str) -> IResult<&str, &str> {
    is_not(" \t\n\n")(i)
}

pub enum Expression {
    Command(Command),
    Block(Vec<Expression>),
    Subshell(Expression),
    Variable(String),
    Operator(Operator),
    Literal(String),
}

pub enum Operator {
    Pipe, // |
    Read, // $
    Wait, // @
    Redirect, // >
}

pub struct Command {
    pub command: &str,
    pub args: Vec<Expression>,
}

mod tests {
    #[test]
    fn test_command(){
        assert_value("echo foobar\n", Expression::Command(Command {
            command: String::from("echo"),
            args: vec![Expression::Literal(String::from("foobar"))],
        }));
    }

    fn test_subshell(){
        let res = command("(echo foobar)\n");
        assert_value("echo foobar\n", Expression::Subshell(Expression::Command(Command {
            command: String::from("echo"),
            args: vec![Expression::Literal(String::from("foobar"))],
        })));
    }

    fn assert_value(src: &str, v: Expression) {
        let res = parse(src);
        assert_eq!(res, Ok("", v));
    }
}
