use orc::Repl;
use std::io;
use std::process;
use failure::Error;

fn main() -> Result<(), Error> {
    let exit_code = Repl::new(io::stdin()).run();
    process::exit(exit_code)
}
