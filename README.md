# ORCestration SHell (orc.sh)

## Reason

- Shells are too permissive of text as code
- Shells fall apart when the script gets complicated enough
- Full programming languages take more time to write and don't take proper
  advantage of the unix philosophy (standalone simple scripts)
- Bash has several gotchas that will cause serious bugs if you're not careful.
- No shell language that I've found treats subprocesses correctly

## Design goals

- Syntax should be bash compatible unless there's a good reason to break it.
- Subprocesses (PIDs) should be first-class system items, not magic $! type
  variables
- Aims to sit half way between a real programming language and a simple script
  system.
- Does *not* aim to be a full or necessarily consistent programming language.
  Scripting speed and ergonomics trumps everything.
- (later) It should be simple to drop into a full programming language when it's
  necessary. Maybe through a plugin system. Will review case-by-case
- Text substitution should be sane by default (no wrapping everything in "" to
  avoid extra spaces bugs). Text as code should be opt in with an eval command.
- Processes should be easy to coordinate
- All child processes should exit on orc exit unless explicitly daemonized
- It should also be simple to schedule cleanup of everything before exit.
- Should have an explicit module system (with web-based imports)
- Should understand JSON natively
- (maybe) Pipes should be dynamically reassignable

## Features

### First-class PIDs

```orc
# Commands switch from inline mode to subprocsess mode by wrapping in ()
set webserver (python -m http.server)
sleep 10
kill webserver.pid()
```

### Long running task management

```orc
python_install = ({
	apt-get install python
	pip install arrow
})
rust_install = ({
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	rustup update
})
@python_install
@rust_install
echo "everything is installed"
```

### Real arrays

```orc
set tasks = []
tasks.push ({
	apt-get install python
	pip install arrow
})
tasks.push ({
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	rustup update
})
tasks.each {
	Waiting for @(pid $1)
	@$1
}
```

## Todo list
* Basic shell syntax
- Pipes
- Variables
- Subprocesses
- Variable substitution
- Arrays
